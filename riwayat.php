<?php
include "koneksi.php";
session_start();
if(empty($_SESSION['username'])){
   header('location:login.php');
}
else{
  $query_pelanggan = mysqli_query($koneksi,"SELECT *FROM pelanggan WHERE username='$_SESSION[username]'");
  $pelanggan = mysqli_fetch_array($query_pelanggan);
}
?>  
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Pembayaran Listrik</title>
  <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
   
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style.css">
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">NPLN</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><?php echo $pelanggan['nama_pelanggan'];?> <a href="logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
        <li class="text-center">
                    <img src="assets/img/user.png" class="user-image img-responsive"/>
          </li>
        
          
                     <li  >
                        <a  href="index.php"><i class="fa fa-credit-card fa-3x"></i>Cek Tagihan</a>
                    </li>              
                    
                                       
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i>Master Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                          <li>
                                <a href="saldo.php">Saldo</a>
                            </li>
                            <li>
                                <a href="riwayat_tagihan.php">Riwayat Tagihan</a>
                            </li>
                            <li>
                                <a href="riwayat.php">Riwayat Pembayaran</a>
                            </li>
                        </ul>
                      </li>    
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                  <div class="col-md-12">
                    
                  
                      <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
              <div class="col-sm-12">
          <h3 align="center">Riwayat Pembayaran</h3>
          <br>
          <br>
          <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
              <tr>
                <th>No</th>
                <th>Tanggal Pembayaran</th>
                <th>Bulan Bayar</th>
                <th>Jumlah Bayar</th>
                <th>Total Bayar</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $no=1;
              $tampil = mysqli_query($koneksi,"SELECT * FROM pembayaran WHERE id_pelanggan='$_SESSION[id_pelanggan]'");
              while($data = mysqli_fetch_array($tampil)){  
                ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $data['tanggal_pembayaran'];?></td>
                  <td>
                    <?php
                  switch($data['bulan_bayar']){
                    case "1";
                    $bulan_bayar = "Januari";
                    break;
                    case "2";
                    $bulan_bayar = "Februari";
                    break;
                    case "3";
                    $bulan_bayar = "Maret";
                    break;
                    case "4";
                    $bulan_bayar = "April";
                    break;
                    case "5";
                    $bulan_bayar = "Mei";
                    break;
                    case "6";
                    $bulan_bayar = "Juni";
                    break;
                    case "7";
                    $bulan_bayar = "Juli";
                    break;
                    case "8";
                    $bulan_bayar = "Agustus";
                    break;
                    case "9";
                    $bulan_bayar = "September";
                    break;
                    case "10";
                    $bulan_bayar = "Oktober";
                    break;
                    case "11";
                    $bulan_bayar = "November";
                    break;
                    case "12";
                    $bulan_bayar = "Desember";
                    break;
                  }
                  echo $bulan_bayar;?>
                  </td>
                  <td><?php echo $data['jumlah_bayar'];?></td>
                  <td><?php echo $data['total_bayar'];?></td>
                  <td>
                    <a href="detail.php"><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-eye-open"></span>Detail</button></a>

                  </td>
                </tr>
                <?php
              }
              ?>
            </tbody>

          </table>

        </div>
      </div> </div>
</div>
                       </div>
                       </div>
                    </div>
                  </div>
              </div>
          
                <!-- /. ROW  -->
            
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
