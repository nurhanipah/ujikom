 <?php
 include "koneksi.php";
 session_start();
 if(empty($_SESSION['username'])){
   header('location:login.php');
 }
 else{
  $query_pelanggan = mysqli_query($koneksi,"SELECT *FROM pelanggan WHERE username='$_SESSION[username]'");
  $pelanggan = mysqli_fetch_array($query_pelanggan);
  $query_pembayaran = mysqli_query($koneksi,"SELECT *FROM pembayaran WHERE id_pelanggan='$_SESSION[id_pelanggan]'");
  $pembayaran = mysqli_fetch_array($query_pembayaran);
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Pembayaran Listrik</title>
  <!-- BOOTSTRAP STYLES-->  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="assets/css/font-awesome.css" rel="stylesheet" />
  <!-- MORRIS CHART STYLES-->
  <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!-- GOOGLE FONTS-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand">NPLN</a> 
      </div>
      <div style="color: white;
      padding: 15px 50px 5px 50px;
      float: right;
      font-size: 16px;"><?php echo $pelanggan['nama_pelanggan'];?> <a href="logout.php" class="btn btn-danger square-btn-adjust">Logout</a> 
    </div>
  </nav>   
  <!-- /. NAV TOP  -->
  <nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
      <ul class="nav" id="main-menu">
        <li class="text-center">
          <img src="assets/img/user.png" class="user-image img-responsive"/>
        </li> 

        <li  >
          <a  href="index.php"><i class="fa fa-credit-card fa-3x"></i>Cek Tagihan</a>
        </li>              


        <li>
          <a href="#"><i class="fa fa-sitemap fa-3x"></i>Master Data<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li>
              <a href="saldo.php">Saldo</a>
            </li>
            <li>
              <a href="riwayat_tagihan.php">Riwayat Tagihan</a>
            </li>
            <li>
              <a href="riwayat.php">Riwayat Pembayaran</a>
            </li>
          </ul>
        </li>    
      </ul>

    </div>

  </nav>  
  <!-- /. NAV SIDE  -->
  <div id="page-wrapper" >
    <div id="page-inner">
      <div class="row">
        <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-12">
                
              
            <h3 align="center">DETAIL PEMBAYARAN</h3>
            <br>
            <br>
            <form class="form-horizontal">    
              <div class="form-group">
                <label class="col-sm-2 control-label">Nama Pelanggan</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="nama_pelanggan" value="<?php echo $pelanggan['nama_pelanggan']?>" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Nomor Kwh</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="nomor_kwh" value="<?php echo $pelanggan['nomor_kwh']?>" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Jumlah Bayar</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="jumlah_bayar" value="<?php echo $pembayaran['jumlah_bayar']?>" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Biaya Admin</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="biaya_admin" value="<?php echo $pembayaran['biaya_admin']?>" readonly>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Total Bayar</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="total_bayar" value="<?php echo $pembayaran['total_bayar']?>" readonly>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="button" class="btn btn-default" onClick="print_d()">Cetak Struk</button>
                  <a href="riwayat.php"><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-"></span>Kembali </button></a>
                </div>
              </div>
            </form>
          </div>
        </div>
        </div>
</div>
            </div>
      </div>
    </div>
    <script>
      function print_d(){
       window.open("prints.php","_blank");
     }
   </script>

   <script src="assets/js/jquery-1.10.2.js"></script>
   <!-- BOOTSTRAP SCRIPTS -->
   <script src="assets/js/bootstrap.min.js"></script>
   <!-- METISMENU SCRIPTS -->
   <script src="assets/js/jquery.metisMenu.js"></script>
   <!-- MORRIS CHART SCRIPTS -->
   <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
   <script src="assets/js/morris/morris.js"></script>
   <!-- CUSTOM SCRIPTS -->
   <script src="assets/js/custom.js"></script>
 </body>
 </html>
