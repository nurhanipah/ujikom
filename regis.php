<?php
include "koneksi.php";
?>
<br>
<br>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Pembayaran Listrik</title>
  <!-- BOOTSTRAP STYLES-->  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="assets/css/font-awesome.css" rel="stylesheet" />
  <!-- MORRIS CHART STYLES-->
  <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!-- GOOGLE FONTS-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
<div class="container">
	<div class="row">
	<div class="col-sm-6">
	<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Registrasi</h3>
  </div>
  <div class="panel-body">
  <form action="tambah.php" method="POST">
    <div class="form-group row" >
      <label class="col-sm-4 col-form-label">Username</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="username" id="username" placeholder="username" autocomplete="off" required>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-4 col-form-label">Password</label>
      <div class="col-sm-8">
        <input type="password" class="form-control" name="password" id="Password" placeholder="Password" autocomplete="off" required>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-4 col-form-label">Email</label>
      <div class="col-sm-8">
        <input type="email" class="form-control" name="email" id="Password" placeholder="Email" autocomplete="off" required>
      </div>
    </div>
    <div class="form-group row">
      <label  class="col-sm-4 col-form-label">Nomor Kwh</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="nomor_kwh" id="nomor_kwh" placeholder="Nomor Kwh" autocomplete="off" required>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-4 col-form-label">Nama Lengkap</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="nama_pelanggan" id="nama_pelanggan" placeholder="Nama Lengkap" autocomplete="off" required>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-4 col-form-label">Alamat</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="alamat" placeholder="Alamat" autocomplete="off" required>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-sm-4 col-form-label">Id Tarif</label>
      <div class="col-sm-8">
      <select class="form-control" name="id_tarif">
        <?php
        include "koneksi.php";
        $query_tarif = mysqli_query($koneksi,"SELECT * FROM tarif");
        while ($tarif = mysqli_fetch_array($query_tarif)){?>
        <option value="<?php echo $tarif['id_tarif'];?>"><?php echo $tarif['daya'],"watt - ",$tarif['tarifperkwh']." per kwh ";?></option>

        <?php
        }
        ?>
      </select>
      </div>
      </div>
    <div class="form-group row">
      <div class="col-sm-10">
        <button type="submit" class="btn btn-primary" name="simpan">SIGN IN</button>
      </div>
    </div>
    </div>
    
  </form>
  <div class="panel-footer">Panel footer</div>
    </div>
</div>
  </div>

</div>
</div>


<script src="assets/js/jquery-1.10.2.js"></script>
  <!-- BOOTSTRAP SCRIPTS -->
  <script src="assets/js/bootstrap.min.js"></script>
  <!-- METISMENU SCRIPTS -->
  <script src="assets/js/jquery.metisMenu.js"></script>
  <!-- MORRIS CHART SCRIPTS -->
  <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
  <script src="assets/js/morris/morris.js"></script>
  <!-- CUSTOM SCRIPTS -->
  <script src="assets/js/custom.js"></script>
</body>
</html>