<?php 
session_start();
include "koneksi.php";
$today = date("Ymd"); //untuk mengambil tahun, tanggal dan bulan hari ini
$tanggal_daftar = date("d/m/Y");
//cari id terakhir ditanggal hari ini
$query1 = "SELECT max(id_pembayaran) as maxID FROM pembayaran WHERE id_pembayaran LIKE '$today%'";
$hasil = mysqli_query($koneksi, $query1);
$data = mysqli_fetch_array($hasil);
$idMax = $data['maxID'];

//setelah membaca id terakhir , lanjut mencari nomor urut id dari id terakhir
$NoUrut = (int) substr($idMax, 8, 3);
$NoUrut++; //nomor urut +1

//setelah ketemu id terakhir lanjut membuat id baru dengan format sbb:
$NewID = $today .sprintf('%03s',$NoUrut);
//$today nanti jadinya misal 20160526 .sprintf('%03s', $NoUrut) urutan id di tanggal hari ini

date_default_timezone_set('Asia/Jakarta');
$tanggal_pembayaran = date('Y/m/d H:i:s');
$bulan_bayar = date('n');
$query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE id_pelanggan = '$_SESSION[id_pelanggan]'");
$pelanggan = mysqli_fetch_array($query_pelanggan);
$id_tagihan = $_GET['id_tagihan'];
$query_tagihan = mysqli_query($koneksi, "SELECT * FROM tagihan WHERE id_tagihan = '$id_tagihan'");
$tagihan = mysqli_fetch_array($query_tagihan);
$jumlah_bayar = $_GET['jumlah_bayar'];
$status = $_GET['status'];
$total_bayar = $_GET['total_bayar'];
$metode_pembayaran = $_GET['metode'];
$jumlah_denda = $_GET['jumlah_denda'];
  ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Pembayaran Listrik</title>
  <!-- BOOTSTRAP STYLES-->  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="assets/css/font-awesome.css" rel="stylesheet" />
  <!-- MORRIS CHART STYLES-->
  <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!-- GOOGLE FONTS-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand">NPLN</a> 
      </div>
      <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><?php echo $pelanggan['nama_pelanggan'];?> <a href="logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
    </nav>   
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
          <li class="text-center">
            <img src="assets/img/user.png" class="user-image img-responsive"/>
          </li>

          <li  >
            <a  href="index.php"><i class="fa fa-credit-card fa-3x"></i>Cek Tagihan</a>
          </li>              


          <li>
            <a href="#"><i class="fa fa-sitemap fa-3x"></i>Master Data<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="saldo.php">Saldo</a>
              </li>
              <li>
                <a href="riwayat_tagihan.php">Riwayat Tagihan</a>
              </li>
              <li>
                <a href="riwayat.php">Riwayat Pembayaran</a>
              </li>
            </ul>
          </li>    
        </ul>

      </div>

    </nav>  
    <!-- /. NAV SIDE  -->

    <div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">  
           <h3 align="center">Pembayaran Bank</h3>

         </div>

       </div>
       <!-- /. ROW  -->
       <hr />

       <?php
       include 'koneksi.php';
       $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan WHERE username='$_SESSION[username]'");
       $pelanggan = mysqli_fetch_array($query_pelanggan);{
       ?>

       <div class="row">
        <div class="col-md-12">
          <!-- Form Elements -->
          <div class="panel panel-default">
            <div class="panel-heading">
              Detail
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                  <form action="proses_saldo.php" method="POST">
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Nama Pelanggan</label>
                      <div class="col-sm-9">
                        <input type="text" name="username" class="form-control" value="<?php echo $pelanggan['nama_pelanggan']; ?>" placeholder="Nama Pelanggan" readonly>
                      </div>
                    </div>
                    <?php
                    $query_pembayaran = mysqli_query($koneksi, "SELECT * FROM pembayaran");
                    $pembayaran = mysqli_fetch_array($query_pembayaran);{
                    ?>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Metode Pembayaran</label>
                      <div class="col-sm-9">
                        <input type="text" name="metode_pembayaran" class="form-control" value="<?php echo $metode_pembayaran;?>" placeholder="Metode Pembayaran" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Nomor Rekening</label>
                      <div class="col-sm-9">
                        <input type="text" name="" class="form-control" value="32467648" placeholder="Nomor Rekening" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Atas Nama</label>
                      <div class="col-sm-9">
                        <input type="text" name="" class="form-control" value="Nur hanipah" placeholder="Atas Nama" readonly>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Total Bayar</label>
                      <div class="col-sm-9">
                        <input type="text" name="jumlah_isi" class="form-control" value="<?php echo $total_bayar; ?>" placeholder="Total Bayar" readonly>
                      </div>
                    </div>
                    <?php
                  }
                    ?>
                    <?php
                    $query_tagihan = mysqli_query($koneksi, "SELECT * FROM tagihan");
                    $tagihan = mysqli_fetch_array($query_tagihan);
                    {
                    ?>
                    <div class="form-group row">
                      <label class="col-sm-3 col-form-label">Status</label>
                      <div class="col-sm-9">
                        <input type="text" name="status" class="form-control" value="<?php echo $tagihan['status'];?>" placeholder="Status" readonly>
                      </div>
                    </div>
                    <?php
                  }
                  ?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php
}
?>

    </div>
    <!-- /. PAGE INNER  -->
  </div>
  <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>


</body>
</html>
