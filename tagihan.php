<?php
include "koneksi.php";
session_start();
$today = date("Ymd");
$tanggal_daftar = date("d/m/y");
$query1 = "SELECT max(id_tagihan) as maxID FROM tagihan WHERE id_tagihan LIKE '$today%'";
$hasil= mysqli_query($koneksi,$query1);
$data= mysqli_fetch_array($hasil);
$idMax= $data['maxID'];

$NoUrut = (int) substr($idMax, 8, 3);
$NoUrut++;

$NewID = $today .sprintf('%03s',$NoUrut);

$id_pelanggan =$_SESSION['id_pelanggan'];
$query_penggunaan = mysqli_query($koneksi,"SELECT * FROM penggunaan WHERE id_pelanggan='$id_pelanggan' order by bulan desc");
$penggunaan = mysqli_fetch_array($query_penggunaan);
$id_penggunaan = $penggunaan['id_penggunaan'];

$query_pelanggan = mysqli_query($koneksi,"SELECT * FROM pelanggan WHERE id_pelanggan='$id_pelanggan'");
$pelanggan = mysqli_fetch_array($query_pelanggan);
$nama_pelanggan =$pelanggan['nama_pelanggan'];
$id_tarif =$pelanggan['id_tarif'];


$query_tarif = mysqli_query($koneksi,"SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
$tarif = mysqli_fetch_array($query_tarif);
$tarifdaya =$tarif['daya'];
$tarifperkwh =$tarif['tarifperkwh'];

$bulan_tagihan = $penggunaan['bulan'];
$tahun_tagihan = $penggunaan['tahun'];
$jumlah_meter_penggunaan =$penggunaan['meter_akhir']-$penggunaan['meter_awal'];
$jumlah_tagihan = $jumlah_meter_penggunaan * $tarifperkwh; 
$tahun_cek = date("Y");
$bulan_cek = date('n');
$tanggal_denda = date('d');
if ($tanggal_denda > 20) {
   $jumlah_denda = 5000;
 } else{
  $jumlah_denda = 0;
 }
 $query_tagihan_belum_bayar = mysqli_query($koneksi,"SELECT * FROM tagihan WHERE id_pelanggan='$id_pelanggan' and tahun=$tahun_cek and status='Belum Dibayar'");
 $cek_tagihan_belum_dibayar = mysqli_num_rows($query_tagihan_belum_bayar);
 if($cek_tagihan_belum_dibayar > 0){
  echo "<script>window.alert('Masih ada tagihan yang belum lunas')</script>";
  $tagihan = mysqli_fetch_array($query_tagihan_belum_bayar);
  $status = $tagihan['status'];
  $id_tagihan = $tagihan['id_tagihan'];
 }
 else{

$query_tarif = mysqli_query($koneksi,"INSERT INTO tagihan VALUES ('$NewID','$id_penggunaan','$id_pelanggan','$bulan_tagihan','$tahun_tagihan','$jumlah_meter_penggunaan','Belum Dibayar')");
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Pembayaran Listrik</title>
    <!-- BOOTSTRAP STYLES-->  <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style.css">
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">NPLN</a>
                </div> 
            <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><?php echo $pelanggan['nama_pelanggan'];?> <a href="logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                <li class="text-center">
                    <img src="assets/img/user.png" class="user-image img-responsive"/>
                    </li>
                  
                     <li  >
                        <a  href="index.php"><i class="fa fa-credit-card fa-3x"></i>Cek Tagihan</a>
                    </li>              
                    
                                       
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i>Master Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                           <li>
                                <a href="saldo.php">Saldo</a>
                            </li>
                            <li>
                                <a href="riwayat_tagihan.php">Riwayat Tagihan</a>
                            </li>
                            <li>
                                <a href="riwayat.php">Riwayat Pembayaran</a>
                            </li>
                        </ul>
                      </li>      
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                  <div class="col-md-12">
                    <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-10">
              
           
          <h3 align="center">Form Pembayaran</h3>
          <form class="form-horizontal" method="POST" action="pembayaran_proses.php?p=<?php echo $NewID;?>">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">Nama Pelanggan</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="nama_pelanggan" value="<?php echo $pelanggan['nama_pelanggan']?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Nomor Kwh</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="nomor_kwh" value="<?php echo $pelanggan['nomor_kwh']?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Daya</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="daya" value="<?php echo $tarif['daya']?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Tarif Per-Kwh</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="tarifperkwh" value="<?php echo $tarif['tarifperkwh']?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Bulan Tagihan</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="bulan" value="<?php echo $penggunaan['bulan']?><?php echo $penggunaan['tahun']?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Jumlah Tagihan</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="jumlah_tagihan" value="<?php echo $jumlah_tagihan;?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Denda</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="denda" value="<?php echo $jumlah_denda;?>" readonly>
              </div>
            </div>

            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Status</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="status" value="belum dibayar" readonly>
              </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Metode Pembayaran</label>
                <div class="col-sm-9">
                  <select class="form-control" name="metode_pembayaran">
                    <option value="saldo">Saldo</option>
                    <option value="BRI">Bank BRI</option>
                    <option value="Mandiri">Bank Mandiri</option>
                    <option value="BNI">Bank BNI</option>
                  </select>
                </div>
              </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-default" name="bayar" value="Bayar">
              </div>
            </div>
          </form>
        </div>
      </div>
      </div>
       </div>
          </div>
                    </div>
                </div>
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
     <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
