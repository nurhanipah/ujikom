<?php
include "koneksi.php";
session_start();
if(isset($_GET['pesan'])){
	if($_GET['pesan']=="gagal"){
		echo "<div class='alert'>Username dan Password tidak sesuai</div>";
	}
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>LOGIN || Pembayaran Listrik</title>
  <!-- BOOTSTRAP STYLES-->  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="assets/css/font-awesome.css" rel="stylesheet" />
  <!-- MORRIS CHART STYLES-->
  <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/style_login.css">
  <!-- GOOGLE FONTS-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
	<br>
	<br>

	<div class="container" >
		<div class="row">
		<div class="col-md-4" style="margin-left: 30%">
			<div class="panel panel-warning top150">
			<div class="panel-heading text-center"><b><i class="fa fa-sign-in"></i> Halaman Login</b></div>
				<div class="panel-body">

					<form action="proses_login.php" method="POST">
						<br>
						<div class="input-group">
								<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
								<input type="text" class="form-control" name="username" placeholder="Username" aria-describedby="basic-addon1" 
								autocomplete="off" required>
							</div>
							
							<div class="input-group">
							  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock"></i></span>
							  <input type="password" class="form-control" name="password" placeholder="Password" aria-describedby="basic-addon1"
							  autocomplete="off" required>
							</div>
								<a href="forgot.php" style="padding-left: 12%">Forget Password?</a>
							
						



						<div class="form-group row" style="padding-left: 36%;>
							<div class="col-sm-10">
								<a href="Manual Book PPOB.pdf"><u>Help?</u></a>

								<a href="regis.php"><u>Buat akun?</u></a>

								<input type="submit" name="submit" class="btn btn-default" value="Login">
							</div>

							</form>
							</div>
						</div>
					</div>

				</div>
				</div>

		




		<script src="assets/js/jquery-1.10.2.js"></script>
  <!-- BOOTSTRAP SCRIPTS -->
  <script src="assets/js/bootstrap.min.js"></script>
  <!-- METISMENU SCRIPTS -->
  <script src="assets/js/jquery.metisMenu.js"></script>
  <!-- MORRIS CHART SCRIPTS -->
  <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
  <script src="assets/js/morris/morris.js"></script>
  <!-- CUSTOM SCRIPTS -->
  <script src="assets/js/custom.js"></script>

	</body>
	</html>