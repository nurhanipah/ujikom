<?php
include 'koneksi.php';
session_start();
if (empty($_SESSION['username'])) {
    // header('location:login.php');
  }
  else{
    $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan where username='$_SESSION[username]'");
    $pelanggan = mysqli_fetch_array($query_pelanggan);
  }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Pembayaran Listrik</title>
  <!-- BOOTSTRAP STYLES-->  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="assets/css/font-awesome.css" rel="stylesheet" />
  <!-- MORRIS CHART STYLES-->
  <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!-- GOOGLE FONTS-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
  <div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand">NPLN</a> 
      </div>
    </nav>   
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
      <div class="sidebar-collapse">

      </div>

    </nav>
<div id="page-wrapper" >
      <div id="page-inner">
        <div class="row">
          <div class="col-md-12">  
           <h4 align="center">CETAK STRUK</h4>

         </div>

       </div>
       <!-- /. ROW  -->
       <hr />

        <?php
        $query_pelanggan = mysqli_query($koneksi, "SELECT * FROM pelanggan");
        $pelanggan = mysqli_fetch_array($query_pelanggan);

        $id_pelanggan = $_SESSION['id_pelanggan'];
        $query_pembayaran = mysqli_query($koneksi, "SELECT * FROM pembayaran WHERE id_pelanggan='$id_pelanggan'");
        $pembayaran = mysqli_fetch_array($query_pembayaran);{
        ?>

       <div class="row">
        <div class="col-md-12">
          <!-- Form Elements -->
          <div class="panel panel-default">
            <div class="panel-heading">
              STRUK PEMBAYARAN
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                    <h3 align="center">STRUK PEMBELIAN LISTRIK PRABAYAR</h3><br>
                  <div align="center">
                <table>
                  <tr>
                    <td>NOMOR METER</td>
                    <td><?php echo $pelanggan['nomor_kwh']; ?></td>
                    <td>TANGGAL PEMBELIAN</td>
                    <td><?php echo $pembayaran['tanggal_pembayaran']; ?></td>
                  </tr>
                  <tr>
                    <td>ID PELANGGAN</td>
                    <td><?php echo $pelanggan['id_pelanggan']; ?></td>
                    <td>NOMINAL PEMBELIAN</td>
                    <td><?php echo $pembayaran['jumlah_bayar']; ?></td>
                  </tr>
                  <tr>
                    <td>NAMA PELANGGAN</td>
                    <td><?php echo $pelanggan['nama_pelanggan']; ?></td>
                    <td>BIAYA ADMIN</td>
                    <td><?php echo $pembayaran['biaya_admin']; ?></td>
                  </tr>
                  <?php
                    $id_tarif = $pelanggan['id_tarif'];
                    $query_tarif = mysqli_query($koneksi, "SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
                    $tarif = mysqli_fetch_array($query_tarif);
                    {
                    ?>
                    <tr>
                    <td>TARIF/DAYA</td>
                    <td><?php echo $tarif['tarifperkwh']; ?>/<?php echo $tarif['daya']; ?></td>
                    <td>BIAYA DENDA</td>
                    <td><?php echo $pembayaran['denda']; ?></td>
                  </tr>
                  <tr>
                    <td>TOTAL PEMBAYARAN</td>
                    <td><?php echo $pembayaran['total_bayar']; ?></td>
                  </tr>
                  <?php
                    }
                    ?>
                </table>
            </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<?php
}
?>
</div>

</div>
</div>
<script src="assets/js/jquery-1.10.2.js"></script>
  <!-- BOOTSTRAP SCRIPTS -->
  <script src="assets/js/bootstrap.min.js"></script>
  <!-- METISMENU SCRIPTS -->
  <script src="assets/js/jquery.metisMenu.js"></script>
  <!-- MORRIS CHART SCRIPTS -->
  <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
  <script src="assets/js/morris/morris.js"></script>
  <!-- CUSTOM SCRIPTS -->
  <script src="assets/js/custom.js"></script>
<script>
        window.load = print_d();
        function print_d(){
            window.print();
        }
    </script>
<?php
?>