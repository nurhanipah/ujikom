<?php
include '../koneksi.php';
$pilih = mysqli_query ($koneksi, "SELECT * FROM pelanggan");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>ADMIN || Pembayaran Listrik</title>
  <!-- BOOTSTRAP STYLES-->  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
  <!-- FONTAWESOME STYLES-->
  <link href="../assets/css/font-awesome.css" rel="stylesheet" />
  <!-- MORRIS CHART STYLES-->
  <link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
  <!-- CUSTOM STYLES-->
  <link href="../assets/css/custom.css" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <!-- GOOGLE FONTS-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" >NPLN</a> 
            </div>
            <div style="color: white;
            padding: 15px 50px 5px 50px;
            float: right;
            font-size: 16px;"><a href="../logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav> 
    </div>


    <table class="table">
      <thead>
        <hr><h3 align="center">Data Pembayaran</h3>
        <tr>
            <th>No</th>
            <th>Id Pelanggan</th>
            <th>Username</th>
            <th>Nama Pelanggan</th>
            <th>Nomor Kwh</th>
            <th>Alamat</th>
        </tr>
    </thead><tbody>
        <?php
        $no=1;
        while($hasil = mysqli_fetch_array($pilih))
        { 
            ?>
            <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $hasil['id_pelanggan'];?></td>
                <td><?php echo $hasil['username'];?></td>
                <td><?php echo $hasil['nama_pelanggan'];?></td>
                <td><?php echo $hasil['nomor_kwh'];?></td>
                <td><?php echo $hasil['alamat'];?></td>
            </tr>
        </tbody>
        <?php } ?>
    </table>
    <script>
        window.load = print_d();
        function print_d(){
            window.print();
        }
    </script>