 <?php
include "../koneksi.php";
session_start();
if(empty($_SESSION['username'])){
 header('location:../login.php');
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ADMIN || Pembayaran Listrik</title>
    <!-- BOOTSTRAP STYLES-->  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="../assets/css/custom.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../css/style.css">
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" >NPLN</a> 
            </div>
            <div style="color: white;
          padding: 15px 50px 5px 50px;
          float: right;
          font-size: 16px;"><a href="../logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                <li class="text-center">
                    <img src="../assets/img/user.png" class="user-image img-responsive"/>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i>Master Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="index.php">Data Pengguna</a>
                            </li>
                            <li>
                                <a href="tarif.php">Tarif</a>
                            </li>
                            <li>
                                <a href="#">Laporan<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                  <li>
                                        <a href="laporan_pengguna.php">Laporan pengguna</a>
                                    </li>
                                    <li>
                                        <a href="laporan_tagihan.php">Laporan tagihan</a>
                                    </li>
                                    <li>
                                        <a href="laporan_pembayaran.php">Laporan Pembayaran</a>
                                    </li>

                                </ul>
                               
                            </li>
                        </ul>
                      </li>
                      <li >
                        <a  href="verifikasi.php"><i class="fa fa-bookmark fa-3x"></i>Verifikasi Saldo </a>
                    </li>  
                     <li >
                        <a  href="backupdb.php"><i class="fa fa-list fa-3x"></i>Backup Data</a>
                    </li>               
                    
                                       
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                  <div class="col-md-12">

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-12">
                
             
            <h3 align="center">Tarif Penggunaan</h3>
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">Tambah data</button>
            <br>
            <br>
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Id tarif</th>
                    <th>Daya</th>
                    <th>Tarif perkwh</th>
                    <th>Aksi</th>
                  </tr> 
                </thead>
                <tbody>
                  <?php
                  $no=1;
                  $tampil = mysqli_query($koneksi,"SELECT * FROM tarif");
                  while($data = mysqli_fetch_array($tampil)){  
                    ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><?php echo $data['id_tarif'];?></td>
                      <td><?php echo $data['daya'];?></td>
                      <td><?php echo $data['tarifperkwh'];?></td>

                      <td>
                        <a href="edit_tarif.php?id_tarif=<?php echo $data['id_tarif']; ?>"><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span>Edit</button></a>
                        <a href="hapus_tarif.php?id_tarif=<?php echo $data['id_tarif']; ?>"><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-trash"></span>Hapus</button></a>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Data</h4>
              </div>
              <div class="modal-body">
                <form class="form-horizontal" action="tambah_tarif.php" method="POST">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Daya</label>
                    <div class="col-sm-8">
                      <input type="number" class="form-control" name="daya" placeholder="Daya" autocomplete="off" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tarif Perkwh</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="tarifperkwh" placeholder="Tarif Perkwh" autocomplete="off" required>
                    </div>
                  </div>

                  
                  <button type="submit" name="simpan" class="btn btn-default">Simpan</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </form>
              </div>
              <div class="modal-footer">


              </div>
            </div>
          </div>
        </div>
          </div>
        </div>
      </div>

</div>
 </div>
            </div>
    </div>
  </div>
</div>
</div>
 <script src="../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../assets/js/custom.js"></script>

</body>
</html>
