<?php
include "../koneksi.php";
session_start();
if(empty($_SESSION['username'])){
 header('location:../login.php');
}else{
$id_saldo=$_GET['id_saldo'];
$pilih=mysqli_query($koneksi,"SELECT * FROM saldo WHERE id_saldo='$id_saldo'");
$data=mysqli_fetch_array($pilih);
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ADMIN || Pembayaran Listrik</title>
    <!-- BOOTSTRAP STYLES-->  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="../assets/css/custom.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../css/style.css">
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">NPLN</a> 
            </div>
            <div style="color: white;
          padding: 15px 50px 5px 50px;
          float: right;
          font-size: 16px;"><a href="../logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                <li class="text-center">
                    <img src="../assets/img/user.png" class="user-image img-responsive"/>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i>Master Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="index.php">Data Pengguna</a>
                            </li>
                            <li>
                                <a href="tarif.php">Tarif</a>
                            </li>
                            <li>
                                <a href="#">Laporan<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                  <li>
                                        <a href="laporan_pengguna.php">Laporan pengguna</a>
                                    </li>
                                    <li>
                                        <a href="laporan_tagihan.php">Laporan tagihan</a>
                                    </li>
                                    <li>
                                        <a href="laporan_pembayaran.php">Laporan Pembayaran</a>
                                    </li>

                                </ul>
                               
                            </li>
                        </ul>
                      </li>
                      <li >
                        <a  href="verifikasi.php"><i class="fa fa-bookmark fa-3x"></i>Verifikasi Saldo </a>
                    </li>  
                     <li >
                        <a  href="backupdb.php"><i class="fa fa-list fa-3x"></i>Backup Data</a>
                    </li>               
                                       
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
            <div class="col-sm-12">
                
            
            <h3 align="center">Verifikasi</h3>
            <br>
            <br>

            <form role="form" action="" method="POST">
              <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" class="form-control" value="<?=$data['username'];?>" autocomplete="off" required>
              </div>
              <?php
              $pl=mysqli_query($koneksi,"SELECT * FROM pelanggan WHERE username='$data[username]'");
              $pelanggan=mysqli_fetch_array($pl);
              ?>
              <div class="form-group">
                <label>Nama Pelanggan</label>
                <input type="text" name="nama_pelanggan" class="form-control" value="<?=$pelanggan['nama_pelanggan'];?>" autocomplete="off" required>
              </div>
              <div class="form-group">
                <label>Saldo</label>
                <input type="text" name="jumlah_isi" class="form-control" value="<?=$data['jumlah_isi'];?>" autocomplete="off" required>
              </div>
                <button class="btn btn-default" type="submit" name="verif">Verifikasi</button>
            </form>
  
            </div>

            <?php
            if(isset($_POST['verif'])){
                $username=$_POST['username'];
                $nama_pelanggan=$_POST['nama_pelanggan'];
                $jumlah_isi=$_POST['jumlah_isi'];
                if($data['status'] == "Telah di verifikasi"){
                  echo "<script>window.alert('Status telah di verifikasi')
                  window.location='verifikasi.php'</script>";
                }else{



                $saldo=mysqli_query($koneksi,"UPDATE saldo SET username = '$username',jumlah_isi ='$jumlah_isi',status='Telah di verifikasi' WHERE id_saldo='$id_saldo'");
                $pelanggan=mysqli_query($koneksi,"UPDATE pelanggan SET saldo=saldo+'$jumlah_isi' WHERE username='$data[username]'");
                if($saldo AND $pelanggan){
                    echo "<script>window.alert('Data Berhasil Diverifikasi')
                    window.location='verifikasi.php'</script>";
                }else{
                    echo "gagal";
                }
            }
            }
            ?>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
<script src="../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../assets/js/custom.js"></script>

</body>
</html>
