DROP TABLE admin;

CREATE TABLE `admin` (
  `id_admin` char(16) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_admin` varchar(50) NOT NULL,
  `id_level` char(16) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO admin VALUES("1","user","user212","nur hanipah","1");
INSERT INTO admin VALUES("2","admin","admin212","indah fitria","2");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE pelanggan;

CREATE TABLE `pelanggan` (
  `id_pelanggan` varchar(12) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nomor_kwh` varchar(50) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `saldo` int(11) NOT NULL,
  `id_tarif` int(11) NOT NULL,
  PRIMARY KEY (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pelanggan VALUES("1","hani","773ea849bc5c2751110cac86139d3221","8776","nur hanipah","london","170000","1");
INSERT INTO pelanggan VALUES("2","putri","2532783b3939e5b81319a5f39a5919d0","4657678","putri malu","cikoneng","175000","2");



DROP TABLE pembayaran;

CREATE TABLE `pembayaran` (
  `id_pembayaran` varchar(12) NOT NULL,
  `id_tagihan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL,
  `bulan_bayar` varchar(12) NOT NULL,
  `jumlah_bayar` int(20) NOT NULL,
  `biaya_admin` int(11) NOT NULL,
  `total_bayar` int(20) NOT NULL,
  `id_admin` char(16) NOT NULL,
  PRIMARY KEY (`id_pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pembayaran VALUES("20190402001","20190402001","","2019-04-02 16:53:15","4","10000","2500","12500","Hani");
INSERT INTO pembayaran VALUES("20190501001","20190501001","","2019-05-01 16:53:56","5","20000","2500","22500","Hani");
INSERT INTO pembayaran VALUES("20190501002","20190501003","2","2019-05-01 20:08:48","5","50000","2500","52500","Hani");



DROP TABLE penggunaan;

CREATE TABLE `penggunaan` (
  `id_penggunaan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `bulan` int(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `meter_awal` varchar(50) NOT NULL,
  `meter_akhir` varchar(50) NOT NULL,
  PRIMARY KEY (`id_penggunaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO penggunaan VALUES("20190401001","1","4","2019","0","10");
INSERT INTO penggunaan VALUES("20190501001","1","5","2019","10","20");
INSERT INTO penggunaan VALUES("20190501002","2","5","2019","0","50");
INSERT INTO penggunaan VALUES("20190601001","2","6","2019","50","100");
INSERT INTO penggunaan VALUES("20190701001","2","7","2019","100","200");



DROP TABLE saldo;

CREATE TABLE `saldo` (
  `id_saldo` varchar(12) NOT NULL,
  `username` varchar(20) NOT NULL,
  `jumlah_isi` int(10) NOT NULL,
  `metode` varchar(30) NOT NULL,
  `tanggal_pengisian` datetime NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`id_saldo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO saldo VALUES("20190402001","hani","100000","Bank BCA","2019-04-02 14:38:06","Telah di verifikasi");
INSERT INTO saldo VALUES("20190402002","hani","30000","Bank BRI","2019-04-02 15:11:12","Telah di verifikasi");
INSERT INTO saldo VALUES("20190402003","hani","100000","Bank BRI","2019-04-02 15:14:30","Telah di verifikasi");
INSERT INTO saldo VALUES("20190501001","hani","50000","Bank BRI","2019-05-01 16:36:26","Telah di verifikasi");
INSERT INTO saldo VALUES("20190501002","hani","100000","Bank BRI","2019-05-01 17:11:23","Telah di verifikasi");
INSERT INTO saldo VALUES("20190502001","hani","100000","Bank BRI","2019-05-02 16:18:10","Telah di verifikasi");
INSERT INTO saldo VALUES("20190601001","putri","100000","Bank BRI","2019-06-01 20:16:04","Telah di verifikasi");
INSERT INTO saldo VALUES("20190801001","putri","200000","Bank BRI","2019-08-01 16:43:54","Telah di verifikasi");



DROP TABLE tagihan;

CREATE TABLE `tagihan` (
  `id_tagihan` varchar(12) NOT NULL,
  `id_penggunaan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `bulan` varchar(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `jumlah_meter` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tagihan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO tagihan VALUES("20190401001","20190401001","1","4","2019","10","Belum Dibayar");
INSERT INTO tagihan VALUES("20190501001","20190501001","1","5","2019","10","Belum Dibayar");
INSERT INTO tagihan VALUES("20190501002","20190501001","1","5","2019","10","Belum Dibayar");
INSERT INTO tagihan VALUES("20190501003","20190501002","2","5","2019","50","Lunas");
INSERT INTO tagihan VALUES("20190601001","20190601001","2","6","2019","50","Belum Dibayar");
INSERT INTO tagihan VALUES("20190601002","20190601001","2","6","2019","50","Belum Dibayar");
INSERT INTO tagihan VALUES("20190701001","20190701001","2","7","2019","100","Belum Dibayar");
INSERT INTO tagihan VALUES("20190701002","20190701001","2","7","2019","100","Belum Dibayar");
INSERT INTO tagihan VALUES("20190701003","20190701001","2","7","2019","100","Belum Dibayar");



DROP TABLE tarif;

CREATE TABLE `tarif` (
  `id_tarif` int(2) NOT NULL AUTO_INCREMENT,
  `daya` varchar(5) NOT NULL,
  `tarifperkwh` int(6) NOT NULL,
  PRIMARY KEY (`id_tarif`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO tarif VALUES("1","500","5000");
INSERT INTO tarif VALUES("2","900","1000");



