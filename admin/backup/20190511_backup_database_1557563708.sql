DROP TABLE admin;

CREATE TABLE `admin` (
  `id_admin` char(16) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_admin` varchar(50) NOT NULL,
  `id_level` char(16) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO admin VALUES("1","user","user212","nur hanipah","1");
INSERT INTO admin VALUES("2","admin","4cdf49528e686ca0acd2a359fb834168","indah fitria","2");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE pelanggan;

CREATE TABLE `pelanggan` (
  `id_pelanggan` varchar(12) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nomor_kwh` varchar(50) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `saldo` int(11) NOT NULL,
  `id_tarif` int(11) NOT NULL,
  PRIMARY KEY (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pelanggan VALUES("1","hani","773ea849bc5c2751110cac86139d3221","8776","nur hanipah","london","115000","1");
INSERT INTO pelanggan VALUES("2","putri","2532783b3939e5b81319a5f39a5919d0","4657678","putri malu","cikoneng","225000","2");
INSERT INTO pelanggan VALUES("20190511001","indah","0b9fb4a094973c863b6852bbf6977a33","5365675","indah fitria","bogor","77500","2");
INSERT INTO pelanggan VALUES("20190511002","erna","7fae28b166262eaa65df65f5f447a389","65679","erna jelek","ujung kulon","0","1");



DROP TABLE pembayaran;

CREATE TABLE `pembayaran` (
  `id_pembayaran` varchar(12) NOT NULL,
  `id_tagihan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL,
  `bulan_bayar` varchar(12) NOT NULL,
  `jumlah_bayar` int(20) NOT NULL,
  `biaya_admin` int(11) NOT NULL,
  `denda` int(11) NOT NULL,
  `total_bayar` int(20) NOT NULL,
  `id_admin` char(16) NOT NULL,
  PRIMARY KEY (`id_pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pembayaran VALUES("20190511001","20190511001","20190511001","2019-05-11 06:17:34","5","20000","2500","0","22500","Hani");
INSERT INTO pembayaran VALUES("20190511002","20190511002","1","2019-05-11 11:54:14","5","100000","2500","0","102500","Hani");



DROP TABLE penggunaan;

CREATE TABLE `penggunaan` (
  `id_penggunaan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `bulan` int(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `meter_awal` varchar(50) NOT NULL,
  `meter_akhir` varchar(50) NOT NULL,
  PRIMARY KEY (`id_penggunaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO penggunaan VALUES("20190511001","20190511001","5","2019","0","20");
INSERT INTO penggunaan VALUES("20190511002","1","5","2019","0","20");



DROP TABLE saldo;

CREATE TABLE `saldo` (
  `id_saldo` varchar(12) NOT NULL,
  `username` varchar(20) NOT NULL,
  `jumlah_isi` int(10) NOT NULL,
  `metode` varchar(30) NOT NULL,
  `tanggal_pengisian` datetime NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`id_saldo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO saldo VALUES("20190510001","hani","100000","Bank BRI","2019-05-11 01:18:25","Telah di verifikasi");
INSERT INTO saldo VALUES("20190511001","indah","100000","Bank BCA","2019-05-11 06:15:44","Telah di verifikasi");



DROP TABLE tagihan;

CREATE TABLE `tagihan` (
  `id_tagihan` varchar(12) NOT NULL,
  `id_penggunaan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `bulan` varchar(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `jumlah_meter` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tagihan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO tagihan VALUES("20190511001","20190511001","20190511001","5","2019","20","Lunas");
INSERT INTO tagihan VALUES("20190511002","20190511002","1","5","2019","20","Lunas");



DROP TABLE tarif;

CREATE TABLE `tarif` (
  `id_tarif` int(2) NOT NULL AUTO_INCREMENT,
  `daya` varchar(5) NOT NULL,
  `tarifperkwh` int(6) NOT NULL,
  PRIMARY KEY (`id_tarif`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO tarif VALUES("1","500","5000");
INSERT INTO tarif VALUES("2","900","1000");



