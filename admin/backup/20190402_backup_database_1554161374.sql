DROP TABLE admin;

CREATE TABLE `admin` (
  `id_admin` char(16) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_admin` varchar(50) NOT NULL,
  `id_level` char(16) NOT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO admin VALUES("1","user","user212","nur hanipah","1");
INSERT INTO admin VALUES("2","admin","admin212","indah fitria","2");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE pelanggan;

CREATE TABLE `pelanggan` (
  `id_pelanggan` varchar(12) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nomor_kwh` varchar(50) NOT NULL,
  `nama_pelanggan` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `saldo` int(11) NOT NULL,
  `id_tarif` int(11) NOT NULL,
  PRIMARY KEY (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pelanggan VALUES("1","hani","773ea849bc5c2751110cac86139d3221","8776","nur hanipah","london","0","1");



DROP TABLE pembayaran;

CREATE TABLE `pembayaran` (
  `id_pembayaran` varchar(12) NOT NULL,
  `id_tagihan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL,
  `bulan_bayar` varchar(12) NOT NULL,
  `jumlah_bayar` int(20) NOT NULL,
  `biaya_admin` int(11) NOT NULL,
  `total_bayar` int(20) NOT NULL,
  `id_admin` char(16) NOT NULL,
  PRIMARY KEY (`id_pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pembayaran VALUES("20190402001","20190402001","1","2019-04-02 05:09:32","4","280000","2500","282500","Hani");



DROP TABLE penggunaan;

CREATE TABLE `penggunaan` (
  `id_penggunaan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `bulan` int(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `meter_awal` varchar(50) NOT NULL,
  `meter_akhir` varchar(50) NOT NULL,
  PRIMARY KEY (`id_penggunaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO penggunaan VALUES("20190402001","1","4","2019","0","56");



DROP TABLE saldo;

CREATE TABLE `saldo` (
  `id_saldo` varchar(12) NOT NULL,
  `username` varchar(20) NOT NULL,
  `jumlah_isi` int(10) NOT NULL,
  `metode` varchar(30) NOT NULL,
  `tanggal_pengisian` datetime NOT NULL,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`id_saldo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO saldo VALUES("20190311001","hani","30000","Bank BRI","2019-03-11 22:27:03","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190311002","hani","100000","Bank BRI","2019-03-12 04:58:11","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190329001","putri","2000","Bank BRI","2019-03-29 14:43:54","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190329002","putri","30","Bank BRI","2019-03-29 14:49:19","Menunggu Verifikasi");
INSERT INTO saldo VALUES("20190402001","hani","5000000","Bank BRI","2019-04-02 05:12:56","Menunggu Verifikasi");



DROP TABLE tagihan;

CREATE TABLE `tagihan` (
  `id_tagihan` varchar(12) NOT NULL,
  `id_penggunaan` varchar(12) NOT NULL,
  `id_pelanggan` varchar(12) NOT NULL,
  `bulan` varchar(2) NOT NULL,
  `tahun` year(4) NOT NULL,
  `jumlah_meter` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tagihan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO tagihan VALUES("20190402001","20190402001","1","4","2019","56","Lunas");



DROP TABLE tarif;

CREATE TABLE `tarif` (
  `id_tarif` int(2) NOT NULL AUTO_INCREMENT,
  `daya` varchar(5) NOT NULL,
  `tarifperkwh` int(6) NOT NULL,
  PRIMARY KEY (`id_tarif`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO tarif VALUES("1","500","5000");
INSERT INTO tarif VALUES("2","900","1000");
INSERT INTO tarif VALUES("3","300","2000");



