<?php
include "../koneksi.php";
session_start();
if(empty($_SESSION['username'])){
 header('location:../login.php');
}else{
$id_tarif=$_GET['id_tarif'];
$pilih=mysqli_query($koneksi,"SELECT * FROM tarif WHERE id_tarif='$id_tarif'");
$tampil=mysqli_fetch_array($pilih);
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ADMIN || Pembayaran Listrik</title>
    <!-- BOOTSTRAP STYLES-->  <link href="../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="../assets/css/custom.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../css/style.css">
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">NPLN</a> 
            </div>
            <div style="color: white;
          padding: 15px 50px 5px 50px;
          float: right;
          font-size: 16px;"><a href="../logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                <li class="text-center">
                    <img src="../assets/img/user.png" class="user-image img-responsive"/>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i>Master Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="index.php">Data Pengguna</a>
                            </li>
                            <li>
                                <a href="tarif.php">Tarif</a>
                            </li>
                            <li>
                                <a href="#">Laporan<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                  <li>
                                        <a href="laporan_pengguna.php">Laporan pengguna</a>
                                    </li>
                                    <li>
                                        <a href="laporan_tagihan.php">Laporan tagihan</a>
                                    </li>
                                    <li>
                                        <a href="laporan_pembayaran.php">Laporan Pembayaran</a>
                                    </li>

                                </ul>
                               
                            </li>
                        </ul>
                      </li>    
                  
                      <li >
                        <a  href="verifikasi.php"><i class="fa fa-bookmark fa-3x"></i>Verifikasi Saldo </a>
                    </li>  
                     <li >
                        <a  href="backupdb.php"><i class="fa fa-list fa-3x"></i>Backup Data</a>
                    </li>               
                    
                                       
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                  <div class="col-md-12">
          <div class="panel-body">
            <div class="row">
            <div class="col-sm-12">
            <h3 align="center">Edit Tarif</h3>


            <form role="form" action="" method="POST">
              <div class="form-group">
                <label>Id Tarif</label>
                <input type="number" name="id_tarif" class="form-control" value="<?=$tampil['id_tarif'];?>" autocomplete="off" required>
              </div>
              <div class="form-group">
                <label>Daya</label>
                <input type="number" name="daya" class="form-control" value="<?=$tampil['daya'];?>" autocomplete="off" required>
              </div>
              <div class="form-group">
                <label>Tarif Perkwh</label>
                <input type="number" name="tarifperkwh" class="form-control" value="<?=$tampil['tarifperkwh'];?>" autocomplete="off" required>
              </div>
              <button type="submit" name="edit" class="btn btn-default" >Edit</button>
              <button type="submit" name="edit" class="btn btn-default" href="index.php" >Kembali</button>

            </form>
          </div>
        </div>
      </div>

      <?php
      include '../koneksi.php';
      if (isset($_POST['edit'])) {
        $daya=$_POST['daya'];
        $tarifperkwh=$_POST['tarifperkwh'];


        $edit=mysqli_query($koneksi,"UPDATE tarif SET daya='$daya' ,tarifperkwh='$tarifperkwh' WHERE id_tarif='$_GET[id_tarif]' ");

        if ($edit) {
          echo"<script>alert('Data Telah Berhasil Di Edit!');window.location='tarif.php';</script>";
        }else{
          echo "gagal simpan";
        }
      }
      ?>

    </div>

  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<script src="../assets/js/jquery-1.10.2.js"></script>
     <!-- BOOTSTRAP SCRIPTS -->
   <script src="../assets/js/bootstrap.min.js"></script>
   <!-- METISMENU SCRIPTS -->
   <script src="../assets/js/jquery.metisMenu.js"></script>
    <!-- DATA TABLE SCRIPTS -->
   <script src="../assets/js/dataTables/jquery.dataTables.js"></script>
   <script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
       <script>
           $(document).ready(function () {
               $('#dataTables-example').dataTable();
           });
   </script>
        <!-- CUSTOM SCRIPTS -->
   <script src="../assets/js/custom.js"></script>

</body>
</html>
