<?php
include "koneksi.php";
session_start();
if(empty($_SESSION['username'])){
   header('location:login.php');
}
else{
  $query_saldo = mysqli_query($koneksi,"SELECT *FROM saldo WHERE username='$_SESSION[username]'");
  $saldo = mysqli_fetch_array($query_saldo);
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Pembayaran Listrik</title>
    <!-- BOOTSTRAP STYLES-->  <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="css/style.css">
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">NPLN</a> 
              </div>
           <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                <li class="text-center">
                    <img src="assets/img/user.png" class="user-image img-responsive"/>
                    </li>
                  
                      <li  >
                        <a  href="index.php"><i class="fa fa-credit-card fa-3x"></i>Cek Tagihan</a>
                    </li>              
                    
                                       
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i>Master Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                           <li>
                                <a href="saldo.php">Saldo</a>
                            </li>
                            <li>
                                <a href="riwayat_tagihan.php">Riwayat Tagihan</a>
                            </li>
                            <li>
                                <a href="riwayat.php">Riwayat Pembayaran</a>
                            </li>
                        </ul>
                      </li>    
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                  <div class="col-md-12">
                    
                  
                    <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-12">
              
            
          <h3 align="center">Detail Saldo</h3>
          <br>
          <br>

          <form action="proses_saldo.php" method="post" class="form-horizontal">
            <div class="form-group">
              <label class="col-sm-2 control-label">Username</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $saldo['username'];?>" autocomplete="off" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Metode Pembayaran</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" value="<?php echo $saldo['metode'];?>" autocomplete="off" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Tanggal Top-up</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" value="<?php echo $saldo['tanggal_pengisian'];?>" autocomplete="off" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Nomor Rekening</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" value="387854756" autocomplete="off" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Atas Nama</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" value="Nur hanipah" autocomplete="off" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Jumlah yang harus di bayar</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" value="<?php echo $saldo['jumlah_isi'];?>" autocomplete="off" required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Status</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" value="<?php echo $saldo['status'];?>" autocomplete="off" required>
              </div>
            </div>
            
              <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <a href="saldo.php"><button type="button" class="btn btn-default"><span class="glyphicon glyphicon-"></span>Kembali </button></a>
              </div>
            </div>
          </form>
        </div>
        </div>
</div>
          </div>
                </div>
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
     <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
